HOMEPAGE = "https://github.com/containerd/nerdctl"
SUMMARY =  "Docker-compatible CLI for containerd"
DESCRIPTION = "nerdctl: Docker-compatible CLI for containerd \
    "

DEPENDS = " \
    go-md2man \
    ${@bb.utils.filter('DISTRO_FEATURES', 'systemd', d)} \
"

SRCREV_FORMAT="nerdcli_cgroups"
SRCREV_nerdcli = "3668d7d0c2e8712ddf295622ed683f69651d023c"
SRC_URI = "git://github.com/containerd/nerdctl.git;name=nerdcli"

# Dependency SRCREVs are from nerdctl go.mod, until fetcher support for
# go modules is available.
SRCREV_go-winio="5516f17a595844b6011597b41d9573aafabb6b6a"
SRC_URI += "git://github.com/Microsoft/go-winio;name=go-winio;destsuffix=git/src/import/.gopath/src/github.com/Microsoft/go-winio"
SRCREV_compose-go="01e9e6b4c64c6c82ddabf99e2a6a68e958001167"
SRC_URI += "git://github.com/compose-spec/compose-go;name=compose-go;destsuffix=git/src/import/.gopath/src/github.com/compose-spec/compose-go"
SRCREV_cgroups = "198bd73e83ff6bdebc18b1a356bf99df2436140f"
SRC_URI += "git://github.com/containerd/cgroups;name=cgroups;destsuffix=git/src/import/.gopath/src/github.com/containerd/cgroups"
SRCREV_console = "d5efa7d56fd239f7f3dad2ab6685db09359a3618"
SRC_URI += "git://github.com/containerd/console;name=console;destsuffix=git/src/import/.gopath/src/github.com/containerd/console"

SRCREV_containerd="b60331a92d9c1ba601a9d33dc69e8b42bebd27d5"
SRC_URI += "git://github.com/containerd/containerd;branch=release/1.5;name=containerd;destsuffix=git/src/import/.gopath/src/github.com/containerd/containerd"

SRCREV_go-cni="f1cbb273482be741efcf81da9eb69e3ab82a42cf"
SRC_URI += "git://github.com/containerd/go-cni;name=go-cni;destsuffix=git/src/import/.gopath/src/github.com/containerd/go-cni"
SRCREV_imgcrypt="93e954ac574d39a54b4e6a8e9e50b7ff4acee41c"
SRC_URI += "git://github.com/containerd/imgcrypt;name=imgcrypt;destsuffix=git/src/import/.gopath/src/github.com/containerd/imgcrypt"
SRCREV_stargz-snapshotter="17b648dd9cd4a4b859762c07b85d302e166c3387"
SRC_URI += "git://github.com/containerd/stargz-snapshotter;name=stargz-snapshotter;destsuffix=git/src/import/.gopath/src/github.com/containerd/stargz-snapshotter"
SRCREV_typeurl="9e1ad9a145b768c8f6bab9880513cd1b05fe5a20"
SRC_URI += "git://github.com/containerd/typeurl;name=typeurl;destsuffix=git/src/import/.gopath/src/github.com/containerd/typeurl"
SRCREV_cni="9fc34aee0a6396d6484992035819c92c42522eea"
SRC_URI += "git://github.com/containernetworking/cni;name=cni;branch=release-0.8;destsuffix=git/src/import/.gopath/src/github.com/containernetworking/cni"
SRCREV_plugins="acf137b1f7e1c9f9bf1511ec938ed0aca7841f80"
SRC_URI += "git://github.com/containernetworking/plugins;name=plugins;destsuffix=git/src/import/.gopath/src/github.com/containernetworking/plugins"
SRCREV_cli="370c28948e3c12dce3d1df60b6f184990618553f"
SRC_URI += "git://github.com/docker/cli;name=cli;branch=20.10;destsuffix=git/src/import/.gopath/src/github.com/docker/cli"
SRCREV_docker="ce826938232fbee567c8805460a8b2c82dc2e493"
SRC_URI += "git://github.com/docker/docker;name=docker;branch=20.10;destsuffix=git/src/import/.gopath/src/github.com/docker/docker"
SRCREV_go-connections="7395e3f8aa162843a74ed6d48e79627d9792ac55"
SRC_URI += "git://github.com/docker/go-connections;name=go-connections;destsuffix=git/src/import/.gopath/src/github.com/docker/go-connections"
SRCREV_go-units="62a2352f021aa740f52d67afa00d211290c5855c"
SRC_URI += "git://github.com/docker/go-units;name=go-units;destsuffix=git/src/import/.gopath/src/github.com/docker/go-units"
SRCREV_color="bc2269661d49c6c9d0e514eef49710556883f3fa"
SRC_URI += "git://github.com/fatih/color;name=color;destsuffix=git/src/import/.gopath/src/github.com/fatih/color"
SRCREV_protobuf="b03c65ea87cdc3521ede29f62fe3ce239267c1bc"
SRC_URI += "git://github.com/gogo/protobuf;name=protobuf;destsuffix=git/src/import/.gopath/src/github.com/gogo/protobuf"
SRCREV_go-isatty="7b513a986450394f7bbf1476909911b3aa3a55ce"
SRC_URI += "git://github.com/mattn/go-isatty;name=go-isatty;destsuffix=git/src/import/.gopath/src/github.com/mattn/go-isatty"
SRCREV_aec="39771216ff4c63d11f5e604076f9c45e8be1067b"
SRC_URI += "git://github.com/morikuni/aec;name=aec;destsuffix=git/src/import/.gopath/src/github.com/morikuni/aec"
SRCREV_go-digest="4a41a1fddd8208cc64f911e551a7f361716f8ae5"
SRC_URI += "git://github.com/opencontainers/go-digest;name=go-digest;destsuffix=git/src/import/.gopath/src/github.com/opencontainers/go-digest"
SRCREV_image-spec="91d70ae0ee4346875dc7994bf4931b2c1f3cd1f9"
SRC_URI += "git://github.com/opencontainers/image-spec;name=image-spec;destsuffix=git/src/import/.gopath/src/github.com/opencontainers/image-spec"
SRCREV_runtime-spec="1c3f411f041711bbeecf35ff7e93461ea6789220"
SRC_URI += "git://github.com/opencontainers/runtime-spec;name=runtime-spec;destsuffix=git/src/import/.gopath/src/github.com/opencontainers/runtime-spec"
SRCREV_errors="614d223910a179a466c1767a985424175c39b465"
SRC_URI += "git://github.com/pkg/errors;name=errors;destsuffix=git/src/import/.gopath/src/github.com/pkg/errors"
SRCREV_rootlesskit="def3cbf3dccb98d213893e5b67427369ea00b634"
SRC_URI += "git://github.com/rootless-containers/rootlesskit;name=rootlesskit;destsuffix=git/src/import/.gopath/src/github.com/rootless-containers/rootlesskit"
SRCREV_logrus="bdc0db8ead3853c56b7cd1ac2ba4e11b47d7da6b"
SRC_URI += "git://github.com/sirupsen/logrus;name=logrus;destsuffix=git/src/import/.gopath/src/github.com/sirupsen/logrus"
SRCREV_v2="09ac54c2f97f4249282baf266c54a09fab1bef58"
SRC_URI += "git://github.com/urfave/cli;name=v2;destsuffix=git/src/import/.gopath/src/github.com/urfave/cli"

# non direct go.mod dependencies follow
#SRCREV_go-md2man="af8da765f0460ccb1d91003b4945a792363a94ca"
SRCREV_go-md2man="f79a8a8ca69da163eee19ab442bedad7a35bba5a"
SRC_URI += "git://github.com/cpuguy83/go-md2man;name=go-md2man;destsuffix=git/src/import/.gopath/src/github.com/cpuguy83/go-md2man"

SRCREV_distribution="a01c71e2477eea211bbb83166061e103e0b2ec95"
SRC_URI += "git://github.com/distribution/distribution.git;branch=main;name=distribution;destsuffix=git/src/import/.gopath/src/github.com/distribution/distribution/v3"

SRCREV_go-dbus="e6cb346e4ee46b3c65ab915ceeb0f3739c563c9b"
SRC_URI += "git://github.com/godbus/dbus.git;branch=master;name=go-dbus;destsuffix=git/src/import/.gopath/src/github.com/godbus/dbus"
SRCREV_go-systemd="777e73a89cef78631ccaa97f53a9bae67e166186"
SRC_URI += "git://github.com/coreos/go-systemd;branch=master;name=go-systemd;destsuffix=git/src/import/.gopath/src/github.com/coreos/go-systemd"
SRCREV_mergo="29fb3d3bdc5512887f1dc9aedde6a0fed407fa8f"
SRC_URI += "git://github.com/imdario/mergo;branch=master;name=mergo;destsuffix=git/src/import/.gopath/src/github.com/imdario/mergo"
SRCREV_go-shellwords="501b5b2a57d704071ca7e87617237f0e3784e53c"
SRC_URI += "git://github.com/mattn/go-shellwords;branch=master;name=go-shellwords;destsuffix=git/src/import/.gopath/src/github.com/mattn/go-shellwords"
SRCREV_mapstructure="8ebf2d61a8b4adcce25fc9fc9b76e8452a00672f"
SRC_URI += "git://github.com/mitchellh/mapstructure;branch=master;name=mapstructure;destsuffix=git/src/import/.gopath/src/github.com/mitchellh/mapstructure"
SRCREV_gojsonschema="82fcdeb203eb6ab2a67d0a623d9c19e5e5a64927"
SRC_URI += "git://github.com/xeipuuv/gojsonschema;branch=master;name=gojsonschema;destsuffix=git/src/import/.gopath/src/github.com/xeipuuv/gojsonschema"
SRCREV_gojsonreference="31c2caff03f801f717a584e61d992cc8a6b3b050"
SRC_URI += "git://github.com/xeipuuv/gojsonreference;branch=master;name=gojsonreference;destsuffix=git/src/import/.gopath/src/github.com/xeipuuv/gojsonreference"
SRCREV_gojsonpointer="02993c407bfbf5f6dae44c4f4b1cf6a39b5fc5bb"
SRC_URI += "git://github.com/xeipuuv/gojsonpointer;branch=master;name=gojsonpointer;destsuffix=git/src/import/.gopath/src/github.com/xeipuuv/gojsonpointer"
SRCREV_goyaml="7649d4548cb53a614db133b2a8ac1f31859dda8c"
SRC_URI += "git://github.com/go-yaml/yaml.git;branch=v2;name=goyaml;destsuffix=git/src/import/.gopath/src/gopkg.in/yaml.v2"
SRCREV_ocicrypt="c989c0bcefa41e177f5d9f2f3182a7da13df0621"
SRC_URI += "git://github.com/containers/ocicrypt;branch=master;name=ocicrypt;destsuffix=git/src/import/.gopath/src/github.com/containers/ocicrypt"
SRCREV_continuity="bce1c3f9669b6f3e7f6656ee715b0b4d75fa64a6"
SRC_URI += "git://github.com/containerd/continuity;branch=master;name=continuity;destsuffix=git/src/import/.gopath/src/github.com/containerd/continuity"
SRCREV_hcsshim="e811ee705ec77df2ae28857ade553043fb564d91"
SRC_URI += "git://github.com/microsoft/hcsshim.git;branch=master;name=hcsshim;destsuffix=git/src/import/.gopath/src/github.com/Microsoft/hcsshim"
SRCREV_fifo="650e8a8a179d040123db61f016cb133143e7a581"
SRC_URI += "git://github.com/containerd/fifo;branch=master;name=fifo;destsuffix=git/src/import/.gopath/src/github.com/containerd/fifo"
SRCREV_ttrpc="25f5476b0bcb225573b58540b8bea95ede3a2f4b"
SRC_URI += "git://github.com/containerd/ttrpc;branch=master;name=ttrpc;destsuffix=git/src/import/.gopath/src/github.com/containerd/ttrpc"


SRCREV_docker-distribution="f65a33d3c854c2abd534acadeefb487795c88ef3"
SRC_URI += "git://github.com/docker/distribution.git;branch=main;name=docker-distribution;destsuffix=git/src/import/.gopath/src/github.com/docker/distribution"


SRCREV_golang-grpc="62adda2ece5ec803c824c5009b83cea86de5030d"
SRC_URI += "git://github.com/grpc/grpc-go;branch=master;name=golang-grpc;destsuffix=git/src/import/.gopath/src/google.golang.org/grpc"
SRCREV_github-com-docker-docker-credential-helpers="38bea2ce277ad0c9d2a6230692b0606ca5286526"
SRC_URI += "git://github.com/docker/docker-credential-helpers;name=github-com-docker-docker-credential-helpers;destsuffix=git/src/import/.gopath/src/github.com/docker/docker-credential-helpers" 
SRCREV_github-com-docker-go-events="e31b211e4f1cd09aa76fe4ac244571fab96ae47f"
SRC_URI += "git://github.com/docker/go-events;name=github-com-docker-go-events;destsuffix=git/src/import/.gopath/src/github.com/docker/go-events"
SRCREV_github-com-docker-go-metrics="b619b3592b65de4f087d9f16863a7e6ff905973c"
SRC_URI += "git://github.com/docker/go-metrics;name=github-com-docker-go-metrics;destsuffix=git/src/import/.gopath/src/github.com/docker/go-metrics"
SRCREV_github-com-gogo-googleapis="1f0e43f50bc0606e385ffae1bc80b5b231dcd042"
SRC_URI += "git://github.com/gogo/googleapis;name=github-com-gogo-googleapis;destsuffix=git/src/import/.gopath/src/github.com/gogo/googleapis"


SRCREV_github-com-golang-protobuf="ae97035608a719c7a1c1c41bed0ae0744bdb0c6f"
SRC_URI += "git://github.com/golang/protobuf;name=github-com-golang-protobuf;destsuffix=git/src/import/.gopath/src/github.com/golang/protobuf"

SRCREV_github-com-protocolbuffers-protobuf-go="0e358a402f994eaf96a258b7c5c5b3317d4575aa"
SRC_URI += "git://github.com/protocolbuffers/protobuf-go;name=github-com-protocolbuffers-protobuf-go;destsuffix=git/src/import/.gopath/src/google.golang.org/protobuf"

SRCREV_github-com-google-uuid="512b657a42880af87e9f0d863aa6dccf3540d4ba"
SRC_URI += "git://github.com/google/uuid;name=github-com-google-uuid;destsuffix=git/src/import/.gopath/src/github.com/google/uuid"

SRCREV_github-com-gorilla-mux="d07530f46e1eec4e40346e24af34dcc6750ad39f"
SRC_URI += "git://github.com/gorilla/mux;name=github-com-gorilla-mux;destsuffix=git/src/import/.gopath/src/github.com/gorilla/mux"

SRCREV_github-com-klauspost-compress="2748482b33e249fa630682cce17c0796e1f438a2"
SRC_URI += "git://github.com/klauspost/compress;name=github-com-klauspost-compress;destsuffix=git/src/import/.gopath/src/github.com/klauspost/compress"


SRCREV_github-com-mattn-go-colorable="f6c00982823144337e56cdb71c712eaac151d29c"
SRC_URI += "git://github.com/mattn/go-colorable;name=github-com-mattn-go-colorable;destsuffix=git/src/import/.gopath/src/github.com/mattn/go-colorable"

SRCREV_github-com-miekg-pkcs11="dfad6a6c83eeddcecc7ab96f23831ef22dec255e"
SRC_URI += "git://github.com/miekg/pkcs11;name=github-com-miekg-pkcs11;destsuffix=git/src/import/.gopath/src/github.com/miekg/pkcs11"

SRCREV_github-com-moby-locker="281af2d563954745bea9d1487c965f24d30742fe"
SRC_URI += "git://github.com/moby/locker;branch=main;name=github-com-moby-locker;destsuffix=git/src/import/.gopath/src/github.com/moby/locker"

SRCREV_github-com-moby-sys="40883be4345cf291513e2a4112896d393455fdfb"
SRC_URI += "git://github.com/moby/sys;branch=master;name=github-com-moby-sys;destsuffix=git/src/import/.gopath/src/github.com/moby/sys"

SRCREV_github-com-moby-term="df9cb8a406352e9543f2b7cae1c8579b51b8013e"
SRC_URI += "git://github.com/moby/term;branch=master;name=github-com-moby-term;destsuffix=git/src/import/.gopath/src/github.com/moby/term"

SRCREV_github-com-opencontainers-runc="4e50d1c16334d42506f1aa8bfadbdabf785e8acf"
SRC_URI += "git://github.com/opencontainers/runc;branch=master;name=github-com-opencontainers-runc;destsuffix=git/src/import/.gopath/src/github.com/opencontainers/runc"

SRCREV_github-com-opencontainers-selinux="ea2e2c464952b56ce44cb706b8ac21a0e35009d3"
SRC_URI += "git://github.com/opencontainers/selinux;branch=master;name=github-com-opencontainers-selinux;destsuffix=git/src/import/.gopath/src/github.com/opencontainers/selinux"

SRCREV_github-com-pelletier-go-toml="c893dbf25c73fac5239481e56c68239ded430fba"
SRC_URI += "git://github.com/pelletier/go-toml;branch=master;name=github-com-pelletier-go-toml;destsuffix=git/src/import/.gopath/src/github.com/pelletier/go-toml"

SRCREV_github-com-prometheus-client_golang="b89620c4916814c2960f16255c36d1b381cda9e7"
SRC_URI += "git://github.com/prometheus/client_golang;branch=master;name=github-com-prometheus-client_golang;destsuffix=git/src/import/.gopath/src/github.com/prometheus/client_golang"

SRCREV_github-com-russross-blackfriday="cadec560ec52d93835bf2f15bd794700d3a2473b"
SRC_URI += "git://github.com/russross/blackfriday;branch=v2;name=github-com-russross-blackfriday;destsuffix=git/src/import/.gopath/src/github.com/russross/blackfriday/v2"

SRCREV_github-com-googleapis-go-genproto="fb37daa5cd7af01088722b9989cf936c098c975f"
SRC_URI += "git://github.com/googleapis/go-genproto;branch=master;name=github-com-googleapis-go-genproto;destsuffix=git/src/import/.gopath/src/google.golang.org/genproto"

SRCREV_github-com-square-go-jose-git="df70637b7b18539eab9142c5599296dcc26a3f2a"
SRC_URI += "git://github.com/square/go-jose.git;branch=v2;name=github-com-square-go-jose-git;destsuffix=git/src/import/.gopath/src/gopkg.in/square/go-jose.v2"


SRCREV_github-com-beorn7-perks="37c8de3658fcb183f997c4e13e8337516ab753e6"
SRC_URI += "git://github.com/beorn7/perks;branch=master;name=github-com-beorn7-perks;destsuffix=git/src/import/.gopath/src/github.com/beorn7/perks"

SRCREV_github-com-cespare-xxhash="1a548c8655f85f3ffcf3578aa1dc8f922ecb6a98"
SRC_URI += "git://github.com/cespare/xxhash;branch=master;name=github-com-cespare-xxhash;destsuffix=git/src/import/.gopath/src/github.com/cespare/xxhash/v2"

SRCREV_github-com-golang-snappy="33fc3d5d8d990c6bccaf4da1d7bb661e0cc53cb6"
SRC_URI += "git://github.com/golang/snappy;branch=master;name=github-com-golang-snappy;destsuffix=git/src/import/.gopath/src/github.com/golang/snappy"

SRCREV_github-com-prometheus-client_model="0255a22d35ad5661ef7aa89c95fdf5dfd685283f"
SRC_URI += "git://github.com/prometheus/client_model;branch=master;name=github-com-prometheus-client_model;destsuffix=git/src/import/.gopath/src/github.com/prometheus/client_model"

SRCREV_github-com-prometheus-common="c7c397c3e6f8ebc654ceb06b3f9ed69fde7137b2"
SRC_URI += "git://github.com/prometheus/common;branch=main;name=github-com-prometheus-common;destsuffix=git/src/import/.gopath/src/github.com/prometheus/common"


SRCREV_github-com-prometheus-procfs="5162bec877a860b5ff140b5d13db31ebb0643dd3"
SRC_URI += "git://github.com/prometheus/procfs;branch=master;name=github-com-prometheus-procfs;destsuffix=git/src/import/.gopath/src/github.com/prometheus/procfs"


SRCREV_github-com-stefanberger-go-pkcs11uri="78d3cae3a9805d89aa4fa80a362ca944c89a1b99"
SRC_URI += "git://github.com/stefanberger/go-pkcs11uri;branch=master;name=github-com-stefanberger-go-pkcs11uri;destsuffix=git/src/import/.gopath/src/github.com/stefanberger/go-pkcs11uri"

SRCREV_github-com-willf-bitset="59de210119f50cedaa42d175dc88b6335fcf63f6"
SRC_URI += "git://github.com/willf/bitset;branch=master;name=github-com-willf-bitset;destsuffix=git/src/import/.gopath/src/github.com/willf/bitset"

SRCREV_github-com-mozilla-services-pkcs7="432b2356ecb18209c1cec25680b8a23632794f21"
SRC_URI += "git://github.com/mozilla-services/pkcs7;branch=master;name=github-com-mozilla-services-pkcs7;destsuffix=git/src/import/.gopath/src/go.mozilla.org/pkcs7"


SRCREV_github-com-matttproud-golang_protobuf_extensions="c182affec369e30f25d3eb8cd8a478dee585ae7d"
SRC_URI += "git://github.com/matttproud/golang_protobuf_extensions;branch=master;name=github-com-matttproud-golang_protobuf_extensions;destsuffix=git/src/import/.gopath/src/github.com/matttproud/golang_protobuf_extensions"



SRCREV_github-com-shurcooL-sanitized_anchor_name="7bfe4c7ecddb3666a94b053b422cdd8f5aaa3615"
SRC_URI += "git://github.com/shurcooL/sanitized_anchor_name;branch=master;name=github-com-shurcooL-sanitized_anchor_name;destsuffix=git/src/import/.gopath/src/github.com/shurcooL/sanitized_anchor_name"



# golang
SRCREV_golangsync="036812b2e83c0ddf193dd5a34e034151da389d09"
SRC_URI += "git://github.com/golang/sync;branch=master;name=golangsync;destsuffix=git/src/import/.gopath/src/golang.org/x/sync"
SRCREV_golangterm="a79de5458b56c188f4fc267a58014ac25fec956a"
SRC_URI += "git://github.com/golang/term.git;branch=master;name=golangterm;destsuffix=git/src/import/.gopath/src/golang.org/x/term"
SRCREV_golangnet="4163338589ed626ba11276b63facaef1f55349d4"
SRC_URI += "git://github.com/golang/net.git;branch=master;name=golangnet;destsuffix=git/src/import/.gopath/src/golang.org/x/net"
SRCREV_golangcrypto="38f3c27a63bf8d9928ce230b01cab346d1756e88"
SRC_URI += "git://github.com/golang/crypto.git;branch=master;name=golangcrypto;destsuffix=git/src/import/.gopath/src/golang.org/x/crypto"
SRCREV_golangtext="5c7c50ebbd4f5b0d53b9b2fcdbeb92ffb732a06e"
SRC_URI += "git://github.com/golang/text.git;branch=master;name=golangtext;destsuffix=git/src/import/.gopath/src/golang.org/x/text"
SRCREV_golangsys="b0526f3d87448f0401ea3f7f3a81aa9e6ab4804d"
SRC_URI += "git://github.com/golang/sys.git;branch=master;name=golangsys;destsuffix=git/src/import/.gopath/src/golang.org/x/sys"


# patches
SRC_URI += "file://0001-Makefile-allow-external-specification-of-build-setti.patch \
           "

SRC_URI[sha256sum] = "d7b05a9bff34dfb25abe7e5b1e54cf2607f953d91cb33fb231a4775a1a4afa3d"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://src/import/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

GO_IMPORT = "import"

S = "${WORKDIR}/git"

PV = "v0.8.1+git${SRCPV}"

NERDCTL_PKG = "github.com/containerd/nerdctl"

# BUILDTAGS ?= "seccomp varlink \
# ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'systemd', '', d)} \
# exclude_graphdriver_btrfs exclude_graphdriver_devicemapper"

# # overide LDFLAGS to allow podman to build without: "flag provided but not # defined: -Wl,-O1
# export LDFLAGS=""

inherit go goarch
inherit systemd pkgconfig

do_configure[noexec] = "1"

EXTRA_OEMAKE = " \
     PREFIX=${prefix} BINDIR=${bindir} LIBEXECDIR=${libexecdir} \
     ETCDIR=${sysconfdir} TMPFILESDIR=${nonarch_libdir}/tmpfiles.d \
     SYSTEMDDIR=${systemd_unitdir}/system USERSYSTEMDDIR=${systemd_unitdir}/user \
"

PACKAGECONFIG ?= ""

do_compile() {

    	cd ${S}/src/import

	for v in `find . -name 'vendor'`; do
	    echo mv $v $v.hidden
	    echo mv .gopath/src/github.com/docker/docker/vendor.hidden .gopath/src/github.com/docker/docker/vendor
	done

	# rm -rf .gopath
	mkdir -p .gopath/src/"$(dirname "${NERDCTL_PKG}")"
	ln -sf ../../../../../import/ .gopath/src/"${NERDCTL_PKG}"

	export GOPATH="$GOPATH:${S}/src/import/.gopath"
	cd ${S}/src/import/.gopath/src/"${NERDCTL_PKG}"

	# Pass the needed cflags/ldflags so that cgo
	# can find the needed headers files and libraries
	export GOARCH=${TARGET_GOARCH}
	export CGO_ENABLED="1"
	export CGO_CFLAGS="${CFLAGS} --sysroot=${STAGING_DIR_TARGET}"
	export CGO_LDFLAGS="${LDFLAGS} --sysroot=${STAGING_DIR_TARGET}"

	export GO111MODULE=off

	oe_runmake GO=${GO} BUILDTAGS="${BUILDTAGS}" binaries
	exit 1
}

do_install() {
	true
}

FILES_${PN} += " \
    ${systemd_unitdir}/system/* \
    ${systemd_unitdir}/user/* \
    ${nonarch_libdir}/tmpfiles.d/* \
    ${sysconfdir}/cni \
"

